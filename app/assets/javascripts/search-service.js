angular.module('phonecatApp').service('searchService', ['$http', function($http){
    return {
        search: function(name){
            return $http.get('/search?name=' + name);
        }
    }
}]);