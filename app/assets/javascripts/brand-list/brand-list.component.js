angular.
module('brandList').
component('brandList', {
    templateUrl: 'templates/brands_list',
    controller: ['$http', '$log', 'searchService', function PhoneListController($http, $log, searchService) {
            var self = this;
            self.orderProp = 'name';
            self.log = $log;
            self.models = [];
            self.model_info = null;

            $http.get('brand.json').then(function(response) {
               self.phones = response.data;
            });

            self.getModels = function(name) {
                $http.get("brand/show?name="+name).then(function(response) {
                    self.brand_name = name;
                    self.models = response.data;
                });
            };

            self.getModelInfo = function(url) {
                $http.get("model/show?url="+url).then(function(response) {
                    self.model_info = response.data;
                })
            };

            self.search = function(name) {
                searchService.search(name).then(function(response) {
                    self.models = response.data;
                })
            }
        }
    ]
});

