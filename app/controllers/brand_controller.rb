# frozen_string_literal: true
class BrandController < ActionController::API
  before_action :find_manufacturers

  attr_accessor :manufacturers

  def index
    render json: @manufacturers.to_json
  end

  def show
    name = params[:name].to_s
    @brands = @manufacturers.select { |brand| brand.name == name }
    @models = @brands.first.models
    render json: @models.to_json
  end

  private

  def find_manufacturers
    @manufacturers ||= PhoneSites::GsmArena::Connection.get_manufacturers
  end
end
