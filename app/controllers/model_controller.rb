# frozen_string_literal: true
class ModelController < ActionController::API
  before_action :find_manufacturers
  before_action :find_model

  def show
    render json: @info.to_json, status: :ok
  end

  private

  def find_manufacturers
    @manufacturers ||= PhoneSites::GsmArena::Connection.get_manufacturers
  end

  def find_model
    url = params[:url]
    @info = PhoneSites::GsmArena::Connection.get_description(url)
  end
end
