# frozen_string_literal: true
class SearchController < ActionController::API
  def index
    data = PhoneSites::GsmArena::Connection.search(params[:name])
    render json: data, status: :ok
  end
end
