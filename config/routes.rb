Rails.application.routes.draw do
  get 'templates/brands_list'

  get 'model/show'

  get 'brand' => 'brand#index'

  get 'brand/show'

  root 'home#index'

  get 'search' => 'search#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
